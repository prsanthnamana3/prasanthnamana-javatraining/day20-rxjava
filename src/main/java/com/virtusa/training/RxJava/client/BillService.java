package com.virtusa.training.RxJava.client;

import com.virtusa.training.RxJava.model.Bill;
import io.reactivex.Single;

import java.util.List;

public interface BillService {

    public Single<List<Bill>> getBillTransactions(String accountNumber);
}
