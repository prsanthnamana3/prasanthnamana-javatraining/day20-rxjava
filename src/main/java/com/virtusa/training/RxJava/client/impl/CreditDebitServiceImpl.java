package com.virtusa.training.RxJava.client.impl;


import com.virtusa.training.RxJava.client.CreditDebitService;
import com.virtusa.training.RxJava.model.CreditDebit;
import io.reactivex.Single;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CreditDebitServiceImpl implements CreditDebitService {

    private final RestTemplate restTemplate = new RestTemplate();
    @Override
    public Single<List<CreditDebit>> getCreditDebitTransactions(String accountNumber) {
       /* try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        System.out.println("Creating CD Single");
        Single<List<CreditDebit>> creditDebitSingle = Single.create(sub ->
                sub.onSuccess(
                         getResponse(accountNumber)

                ));

        return creditDebitSingle;

    }

    public List<CreditDebit> getResponse(String accountNumber){
        System.out.println("==========Retrieving GET CreditDebit Transactions========= on thread "+Thread.currentThread());
        System.out.println(System.currentTimeMillis());
        return restTemplate.exchange("http://localhost:8085/creditdebit", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<CreditDebit>>() {
                }).getBody();
    }
}
