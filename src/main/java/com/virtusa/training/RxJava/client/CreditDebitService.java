package com.virtusa.training.RxJava.client;

import com.virtusa.training.RxJava.model.CreditDebit;
import io.reactivex.Single;


import java.util.List;

public interface CreditDebitService {

    Single<List<CreditDebit>> getCreditDebitTransactions(String accountNumber);
}
