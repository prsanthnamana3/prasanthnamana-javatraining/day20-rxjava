package com.virtusa.training.RxJava.client.impl;


import com.virtusa.training.RxJava.client.BillService;
import com.virtusa.training.RxJava.model.Bill;
import io.reactivex.Single;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class BillServiceImpl implements BillService {

    private final RestTemplate restTemplate = new RestTemplate();


    @Override
    public Single<List<Bill>> getBillTransactions(String accountNumber) {
       /* try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        System.out.println("Creating Bill Single");
        return Single.create(sub -> sub.onSuccess(getResponse(accountNumber)));

    }

    public List<Bill> getResponse(String accountNumber){
        System.out.println("==========Retrieving GET Bill Transactions========= on thread "+Thread.currentThread());
        System.out.println(System.currentTimeMillis());
        return restTemplate.exchange("http://localhost:8082/bills", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Bill>>() {
                }).getBody();
    }
}
