package com.virtusa.training.RxJava.model;

import java.util.Date;

public class Bill {

    private User user;
    private String id;
    private String type;
    private Date postedDate;
    private String description;
    private String amount;
    private String accountNumber;
    private String payeeName;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Bill(User user, String id, String type, Date postedDate, String description, String amount, String accountNumber, String payeeName) {
        this.user = user;
        this.id = id;
        this.type = type;
        this.postedDate = postedDate;
        this.description = description;
        this.amount = amount;
        this.accountNumber = accountNumber;
        this.payeeName = payeeName;
    }
    public Bill(){}
}
