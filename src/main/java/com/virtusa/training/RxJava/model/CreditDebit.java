package com.virtusa.training.RxJava.model;

import java.util.Date;

public class CreditDebit {
    private User user;
    private String transactionId;
    private String type;
    private Date date;
    private String cardNumber;
    private String description;
    private String amount;

    public CreditDebit(String transactionId, String type, Date date, String cardNumber, String description, String amount) {
        this.transactionId = transactionId;
        this.type = type;
        this.date = date;
        this.cardNumber = cardNumber;
        this.description = description;
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public CreditDebit(){}
}
