package com.virtusa.training.RxJava.model;

import java.util.List;

public class Transaction {

    List<Bill> billTransactions;
    List<CreditDebit> creditDebitTransactions;

    public Transaction(List<Bill> billTransactions, List<CreditDebit> creditDebitTransactions) {
        this.billTransactions = billTransactions;
        this.creditDebitTransactions = creditDebitTransactions;
    }

    public List<Bill> getBillTransactions() {
        return billTransactions;
    }

    public void setBillTransactions(List<Bill> billTransactions) {
        this.billTransactions = billTransactions;
    }

    public List<CreditDebit> getCreditDebitTransactions() {
        return creditDebitTransactions;
    }

    public void setCreditDebitTransactions(List<CreditDebit> creditDebitTransactions) {
        this.creditDebitTransactions = creditDebitTransactions;
    }

    public Transaction(){}
}
