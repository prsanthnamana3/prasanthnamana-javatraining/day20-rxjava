package com.virtusa.training.RxJava.model;

public class User {

    private String accountNumber;

    public User(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public User(){}
}
