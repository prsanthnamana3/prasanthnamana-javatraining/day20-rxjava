package com.virtusa.training.RxJava.service;

import com.virtusa.training.RxJava.client.BillService;
import com.virtusa.training.RxJava.client.CreditDebitService;
import com.virtusa.training.RxJava.model.Bill;
import com.virtusa.training.RxJava.model.CreditDebit;
import com.virtusa.training.RxJava.model.Transaction;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    BillService billService;

    @Autowired
    CreditDebitService creditDebitService;

    public Transaction retrieveUserTransaction(String accountNumber) {
        Single<List<CreditDebit>> creditDebit = creditDebitService.getCreditDebitTransactions(accountNumber).subscribeOn(Schedulers.newThread());
        System.out.println("Created CD single");
        Single<List<Bill>> bill = billService.getBillTransactions(accountNumber).subscribeOn(Schedulers.newThread());
        System.out.println("Created bill single");
        Single<Transaction> aaa = Single.zip(creditDebit,bill,(a,b)->{
            System.out.println("Inside Zip ");
            Transaction trans = new Transaction();
            trans.setBillTransactions(b);
            trans.setCreditDebitTransactions(a);
            return trans;
        });
        System.out.println("Exec Zip");
        Transaction trans = aaa.blockingGet();
        System.out.println("Completed Exec");
        return trans;
    }
}
