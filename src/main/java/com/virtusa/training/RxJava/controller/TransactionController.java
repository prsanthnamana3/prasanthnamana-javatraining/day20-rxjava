package com.virtusa.training.RxJava.controller;

import com.virtusa.training.RxJava.model.Transaction;

import com.virtusa.training.RxJava.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @GetMapping("/transactions/{accountNumber}")
    public Transaction getTransactions(@PathVariable String accountNumber){
        return transactionService.retrieveUserTransaction(accountNumber);
    }
}
